package tests;

import org.junit.Test;
import src.Calculator;

import static org.junit.Assert.*;

public class TestCalculator {

    @Test
    public void testAdd() {
        Calculator calculator = new Calculator();
        assertTrue(calculator.add(4,5) == 9);
        assertTrue(calculator.add(2,-10) == -8);
        assertTrue(calculator.add(10,12) == 22);
        assertTrue(calculator.add(3,5) == 8);
        assertFalse(calculator.add(3, 4) == 8);
    }

    @Test
    public void testReduce() {
        Calculator calculator = new Calculator();
        assertTrue(calculator.reduce(5, 3) == 2);
        assertTrue(calculator.reduce(5, 5) == 0);
        assertTrue(calculator.reduce(1000, 44) == 956);
        assertTrue(calculator.reduce(5, 10) == -5);

    }

    @Test
    public void testMultiply() {
        Calculator calculator = new Calculator();
        assertTrue(calculator.multiply(5, 3) == 15);
        assertTrue(calculator.multiply(5, 5) == 25);
        assertTrue(calculator.multiply(1000, 44) == 44000);
        assertTrue(calculator.multiply(-8, 7) == -56);

    }

    @Test
    public void testDivide() {
        Calculator calculator = new Calculator();
        assertTrue(calculator.divide(25, 5) == 5);
        assertTrue(calculator.divide(7, 7) == 1);
        assertTrue(calculator.divide(1000, 100) == 10);
        assertTrue(calculator.divide(-8, 4) == -2);

    }
}

