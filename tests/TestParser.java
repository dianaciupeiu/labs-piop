package tests;

import org.junit.Test;
import src.Parser;

import java.io.File;
import java.util.LinkedList;
import java.util.Queue;

import static org.junit.Assert.*;

public class TestParser {

    @Test
    public void testParse() {
        Parser parser = new Parser();
        File myObj = new File("C:\\Users\\Administrator\\PPD\\Lab1PIOP\\date\\date2.txt");
        Queue<String> list = new LinkedList<>();
        list.add("5");
        list.add("*");
        list.add("4");
        assertTrue(parser.parse(myObj).equals(list));
    }

    @Test
    public void testParseComplex() {
        Parser parser = new Parser();
        File myObj = new File("C:\\Users\\Administrator\\PPD\\Lab1PIOP\\date\\date3.txt");
        Queue<String> list = new LinkedList<>();
        list.add("7");
        list.add("-");
        list.add("4");
        list.add("+");
        list.add("15");
        list.add("-");
        list.add("8");
        assertTrue(parser.parse(myObj).equals(list));
    }

    @Test
    public void testParseMoreComplex() {
        Parser parser = new Parser();
        File myObj = new File("C:\\Users\\Administrator\\PIOP\\lab1-tema\\src\\date\\date.txt");
        Queue<String> list = new LinkedList<>();
        list.add("(");
        list.add("5");
        list.add("*");
        list.add("4");
        list.add(")");
        list.add("+");
        list.add("7");
        list.add("-");
        list.add("3");
        list.add("-");
        list.add("(");
        list.add("8");
        list.add("/");
        list.add("4");
        list.add(")");
        assertTrue(parser.parse(myObj).equals(list));
    }
}

