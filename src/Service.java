package src;

import src.Calculator;

import java.util.LinkedList;
import java.util.Objects;
import java.util.Queue;

public class Service {
    public String calculateAddReduce(Queue<String> operators, Queue<Integer> numbers) {
        String result = "";
        Calculator calculator = new Calculator();
        Integer nr1 = numbers.peek();
        numbers.remove();
        while(!numbers.isEmpty() && !operators.isEmpty()) {
            String operator = operators.peek();
            Integer nr2 = numbers.peek();
            Integer rez1 = 0;
            if (Objects.equals(operator, "+")) {
                rez1 = calculator.add(nr1, nr2);
            }
            if (Objects.equals(operator, "-")) {
                rez1 = calculator.reduce(nr1, nr2);
            }
            nr1 = rez1;
            result = nr1.toString();
            operators.remove();
            numbers.remove();
        }
        return result;
    }

    public static String calculateBrackets(Queue<String> numbers) {
        String result = "";
        Calculator calculator = new Calculator();
        while(!numbers.isEmpty()) {
            Integer nr1 = Integer.parseInt(numbers.peek());
            numbers.remove();
            String operator = numbers.peek();
            numbers.remove();
            Integer nr2 = Integer.parseInt(numbers.peek());
            if (Objects.equals(operator, "*")) {
                Integer rez1 = calculator.multiply(nr1, nr2);
                result = rez1.toString();
            }
            if (Objects.equals(operator, "/")) {
                Integer rez1 = calculator.divide(nr1, nr2);
                result = rez1.toString();
            }
            numbers.remove();
        }
        return result;
    }

    public Queue<String> multiplyDivide(Queue<String> elements) {
        Queue<String> finalResult = new LinkedList<>();
        while(!elements.isEmpty()) {
            String el = elements.remove();
            if(Objects.equals(el, "(")) {
                Queue<String> result = new LinkedList<>();
                String nr = elements.remove();
                while (!Objects.equals(nr, ")")) {
                    result.add(nr);
                    nr = elements.remove();
                }
                finalResult.add(calculateBrackets(result));
            }
            else {
                finalResult.add(el);
            }
        }
        return finalResult;
    }

    public String separate(Queue<String> elements) {
        Queue<String> operators = new LinkedList<>();
        Queue<Integer> numbers = new LinkedList<>();
        while (!elements.isEmpty()) {
            String el = elements.peek();
            try {
                Integer nr = Integer.parseInt(el);
                numbers.add(nr);
            }
            catch (Exception e) {
                if(e.getMessage().contains("+")) {
                    operators.add("+");
                }
                if(e.getMessage().contains("-")) {
                    operators.add("-");
                }
            }
            elements.remove();
        }
        String result = calculateAddReduce(operators, numbers);
        return  result;
    }
}
